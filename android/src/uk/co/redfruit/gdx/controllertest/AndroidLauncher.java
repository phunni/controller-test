/*
 * Copyright (c) 2018. Paul Hunnisett.  All Rights Reserved.
 * You may not use, distribute or modify this code without permission of the original author.
 */

package uk.co.redfruit.gdx.controllertest;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new ControllerTest(), config);
    }
}
