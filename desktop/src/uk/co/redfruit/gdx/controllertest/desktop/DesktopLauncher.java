/*
 * Copyright (c) 2018. Paul Hunnisett.  All Rights Reserved.
 * You may not use, distribute or modify this code without permission of the original author.
 */

package uk.co.redfruit.gdx.controllertest.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import uk.co.redfruit.gdx.controllertest.ControllerTest;

public class DesktopLauncher {
    // Methods Start
    public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        new LwjglApplication(new ControllerTest(), config);
    }
// Methods End
}
