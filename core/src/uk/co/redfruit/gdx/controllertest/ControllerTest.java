/*
 * Copyright (c) 2018. Paul Hunnisett.  All Rights Reserved.
 * You may not use, distribute or modify this code without permission of the original author.
 */

package uk.co.redfruit.gdx.controllertest;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ControllerTest extends ApplicationAdapter {

    // Variables Start
    private static final String TAG = "ControllerTest";


    SpriteBatch batch;
    Texture img;
// Variables End

    // Methods Start
    @SuppressWarnings("LibGDXUnsafeIterator")
    @Override
    public void create() {
        batch = new SpriteBatch();
        img = new Texture("badlogic.jpg");
        for (Controller controller : Controllers.getControllers()) {
            Gdx.app.log(TAG, controller.getName());
        }
        Controllers.addListener(new ControllerTestListener());
        Gdx.input.setInputProcessor(new GeneralInputAdapter());
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(img, 0, 0);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        img.dispose();
    }
// Methods End

    private class ControllerTestListener extends ControllerAdapter {
        // Methods Start
        @Override
        public boolean buttonDown(Controller controller, int buttonIndex) {
            Gdx.app.log(TAG, "Button pressed: " + buttonIndex);
            return false;
        }

        @Override
        public void connected(Controller controller) {
            Gdx.app.log(TAG, "Controller connection detected: " + controller.getName());
        }

        @Override
        public void disconnected(Controller controller) {
            Gdx.app.log(TAG, "Controller disconnection detected: " + controller.getName());
        }
// Methods End
    }

    private class GeneralInputAdapter extends InputAdapter {
        // Methods Start
        @Override
        public boolean keyDown(int keycode) {
            switch (keycode) {
                case Input.Keys.ESCAPE:
                    Gdx.app.exit();
                    break;
            }
            return false;
        }
// Methods End
    }
}
